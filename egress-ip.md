oc patch netnamespace demosec3 --type=merge -p \
 '{"egressIPs": ["192.168.12.101"]}'

oc patch netnamespace demosec2 --type=merge -p \
 '{"egressIPs": ["192.168.12.102"]}'

oc patch netnamespace demosec3 --type=merge -p \
 '{"egressIPs": []}'

oc patch netnamespace demosec2 --type=merge -p \
 '{"egressIPs": []}'

oc patch hostsubnet master0.openshift.rhcasalab.com --type=merge -p \
 '{"egressCIDRs": ["192.168.12.0/24"]}'
oc patch hostsubnet master1.openshift.rhcasalab.com --type=merge -p \
 '{"egressCIDRs": ["192.168.12.0/24"]}'
oc patch hostsubnet master2.openshift.rhcasalab.com --type=merge -p \
 '{"egressCIDRs": ["192.168.12.0/24"]}'

oc patch hostsubnet master0.openshift.rhcasalab.com --type=merge -p \
 '{"egressIPs": []}'
oc patch hostsubnet master1.openshift.rhcasalab.com --type=merge -p \
 '{"egressIPs": []}'
oc patch hostsubnet master2.openshift.rhcasalab.com --type=merge -p \
 '{"egressCIDRs": ["192.168.12.0/24"]}'

oc patch hostsubnet master0.openshift.rhcasalab.com --type=merge -p \
 '{"egressCIDRs": []}'

oc get hostsubnet
